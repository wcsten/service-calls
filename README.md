# Olist Challenge - Service calls API
This is an application proposed by Olist's tech team at https://github.com/olist/work-at-olist.

The implementation consists in two REST API that exposes two main resources.

### service-calls
`/v1/calls` - Provides an endpoint for submiting call detail records.

### service-billings
`/v1/charges` - Provides an endpoint to calls application post call ended data to process a charge
`/v1/billings`- Provides an endpoint to get billings details

### How this application has been implemented
You can submit call detail records of `start` or `end` types. Whenever a call is completed, that is, when the API receives both start and end records from the same `call_id` an event is triggered to request `service-billings` application to process a bill.

On the other side we have a endpoint in the `service-billings` [code here](https://bitbucket.org/wcsten/service-billings/src/master/) where the application of calls will send a post in the endpoint of `/v1/charges` for events of completed calls type. Whenever it gets an event it gets the billing based on the call's month and year timestamp, following by the current active price rule, calculate the price for the call being handled and then create a new call charge.

### API Documentation
For submiting call detail records through the REST API you can access the documentation at https://calls-api-test.herokuapp.com/docs/

### Running locally
#### Requirements
- A virtualenv with Python 3.7+ installed.
- SQLite3 installed on your machine.

### Cloning

Clone this repo by running  `git clone git@bitbucket.org:wcsten/service-calls.git` on your terminal.

Run `pip install pipenv`

Then cd to `/service-calls`.

Run `pipenv install --dev`

And finaly to run virtualenv run `pipenv shell`

### Creating your local settings file

Create a `.env` file with the following content:

```
SECRET_KEY=mysecret
DEBUG=True
ALLOWED_HOSTS='*'
DATABASE_URL=sqlite:///db.sqlite3
BILLINGS_URL=http://0.0.0.0:5000/v1/charges/
```
### Initial step
For the project run correctly first we need to configure the billing application for both applications to communicate in this follow [service-billings documentation](https://bitbucket.org/wcsten/service-billings/src/master/)
### Installing the application
The command bellow will install the required dependencies for running the project on your virtualenv.

Run `make install`

When billing application is up run `make loaddata` to create initial data in both applications.

Running the applicaton server
Run `make start`. Access the application on your browser at `http://0.0.0.0:8000/docs/`

### Running tests
Run `make test` on your terminal.


### Work environment
- Visual Studio Code
- MacBook Pro (13-inch, 2017) / Mac OS High Sierra
- Pyenv
- Pip
- Flake8
- Django, Django Rest Framework, Python Decouple
