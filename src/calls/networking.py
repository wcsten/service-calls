import requests
from requests.exceptions import ConnectionError, ConnectTimeout, RequestException

from django.conf import settings

from retry import retry

import logging

logger = logging.getLogger('calls')


@retry((ConnectionError, ConnectTimeout, RequestException), tries=3, backoff=2)
def request_create_billing_charge(call_record_data):
    prefix = '[Request service-billings] -'
    call_id = call_record_data['call_id']
    logger.info(
        f'{prefix} Start to request create charge billing application. call_id: {call_id}'
    )
    endpoint = settings.BILLINGS_URL
    response = requests.post(endpoint, data=call_record_data, timeout=10)
    logger.info(
        f'{prefix} service-billings response: {response.status_code}, call_id: {call_id}'
    )
