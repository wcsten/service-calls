from django.db import models


class CallRecordQuerySet(models.QuerySet):

    def get_pair(self, call_id):
        qs = self.filter(call_id=call_id)
        start_call = qs.filter(record_type='start').first()
        end_call = qs.filter(record_type='end').first()
        return start_call, end_call
