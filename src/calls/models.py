from django.db import models

from calls.managers import StartCallRecordManager, EndCallRecordManager
from calls.querysets import CallRecordQuerySet


class CallRecord(models.Model):
    START, END = 'start', 'end'
    CALL_CHOICES = (
        (START, START),
        (END, END),
    )

    record_type = models.CharField('Record Type', max_length=5,
                                   choices=CALL_CHOICES, default=START)
    timestamp = models.DateTimeField('Timestamp', auto_now_add=True)
    call_id = models.PositiveIntegerField('Call_ID', db_index=True)
    source = models.CharField('Source', max_length=11, blank=True, null=True)
    destination = models.CharField('Destination', max_length=11, blank=True, null=True)

    objects = CallRecordQuerySet.as_manager()
    start_calls = StartCallRecordManager()
    end_calls = EndCallRecordManager()

    class Meta:
        verbose_name = 'Call Record'
        verbose_name_plural = 'Call Records'

    def __str__(self):
        return f'CallRecord id: {self.id} call_id: {self.call_id}'

    def get_duration(self, timestamp):
        if self.record_type == CallRecord.START:
            return timestamp - self.timestamp
        else:
            return self.timestamp - timestamp
