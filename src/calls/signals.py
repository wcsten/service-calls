from django.dispatch import receiver
from django.db.models.signals import post_save

from calls.networking import request_create_billing_charge
from calls.models import CallRecord

import logging

logger = logging.getLogger('calls')


@receiver(post_save, sender=CallRecord)
def call_record_handler(sender, instance, **kwargs):
    dispatch_new_completed_call_to_billing_service(instance)


def dispatch_new_completed_call_to_billing_service(call_record):
    prefix = '[Request service-billings] -'
    logger.info(
        f'{prefix} Start to dispatch completed call for call_id: {call_record.call_id}'
    )

    call_record_pair = CallRecord.objects.get_pair(call_record.call_id)
    start_call, end_call = call_record_pair
    if start_call and end_call:
        logger.info(
            f'{prefix} Start and end call pair for call_id: {call_record.call_id}'
        )
        duration = call_record.get_duration(start_call.timestamp)
        call_record_data = {
            'subscriber': start_call.source,
            'destination': start_call.destination,
            'call_id': start_call.call_id,
            'duration': duration,
            'start_at': start_call.timestamp,
            'end_at': end_call.timestamp,
        }
        request_create_billing_charge(call_record_data)
