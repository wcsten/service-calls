from rest_framework.generics import CreateAPIView

from calls.serializers import CallRecordSerializer
from calls.models import CallRecord


class CreateCallRecordAPIView(CreateAPIView):

    queryset = CallRecord.objects.all()
    serializer_class = CallRecordSerializer
