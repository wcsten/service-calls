from rest_framework import status
from rest_framework.test import APITransactionTestCase
from rest_framework.reverse import reverse

from calls.tests.factories import CallRecordFactory


class CreateCallRecordTestCase(APITransactionTestCase):

    def test_create_record(self):
        data = {
            'record_type': 'start',
            'call_id': 2,
            'source': '99988526333',
            'destination': '99988526000',
        }
        url = reverse('calls')
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_with_invalid_source(self):
        data = {
            'record_type': 'start',
            'call_id': 3,
            'source': '111',
            'destination': '11999988888'
        }
        url = reverse('calls')
        response = self.client.post(url, data=data, format='json')
        errors = response.json()['source']
        self.assertEqual(errors[0], 'Invalid phone number.')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_with_invalid_destination(self):
        data = {
            'record_type': 'start',
            'call_id': 3,
            'source': '11999988888',
            'destination': '666'
        }
        url = reverse('calls')
        response = self.client.post(url, data=data, format='json')
        errors = response.json()['destination']
        self.assertEqual(errors[0], 'Invalid phone number.')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_with_missing_required_fields(self):
        data = {
            'record_type': 'start'
        }
        url = reverse('calls')
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_end_record(self):
        data = {
            'record_type': 'end',
            'call_id': 2,
        }
        url = reverse('calls')
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_end_record_error_with_source(self):
        data = {
            'record_type': 'end',
            'call_id': 2,
            'source': '99988526333',
        }
        url = reverse('calls')
        response = self.client.post(url, data=data, format='json')
        errors = response.json()['non_field_errors']
        self.assertEqual(
            errors[0], 'Call 2 end calls can not have destination or source.'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_same_call_id_end(self):
        CallRecordFactory.create(call_id=2, record_type='end')
        data = {
            'record_type': 'end',
            'call_id': 2,
        }
        url = reverse('calls')
        response = self.client.post(url, data=data, format='json')
        errors = response.json()['non_field_errors']
        self.assertEqual(errors[0], 'Call 2 has already been ended.')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_same_call_id_start(self):
        CallRecordFactory.create(call_id=2, record_type='start')
        data = {
            'record_type': 'start',
            'call_id': 2,
            'source': '99988526333',
            'destination': '99988526000',
        }
        url = reverse('calls')
        response = self.client.post(url, data=data, format='json')
        errors = response.json()['non_field_errors']
        self.assertEqual(errors[0], 'Call 2 has already been started.')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_start_call_with_no_source(self):
        data = {
            'record_type': 'start',
            'call_id': 2,
            'destination': '99988526000',
        }
        url = reverse('calls')
        response = self.client.post(url, data=data, format='json')
        errors = response.json()['non_field_errors']
        self.assertEqual(errors[0], 'Call 2 source and destination fields are required.')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_with_source_and_destination_equals(self):
        data = {
            'record_type': 'start',
            'call_id': 2,
            'source': '99988526333',
            'destination': '99988526333',
        }
        url = reverse('calls')
        response = self.client.post(url, data=data, format='json')
        errors = response.json()['non_field_errors']
        self.assertEqual(errors[0], 'Call 2 source and destination can not be equals.')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_method_not_allowed(self):
        url = reverse('calls')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_delete_method_not_allowed(self):
        url = reverse('calls')
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_put_method_not_allowed(self):
        url = reverse('calls')
        response = self.client.put(url, data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_patch_method_not_allowed(self):
        url = reverse('calls')
        response = self.client.patch(url, data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
