from factory.django import DjangoModelFactory
from calls.models import CallRecord


class CallRecordFactory(DjangoModelFactory):
    class Meta:
        model = CallRecord

    record_type = CallRecord.START
    source = '16999996666'
    destination = '16999557777'
