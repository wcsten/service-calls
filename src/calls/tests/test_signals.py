import responses
from unittest import mock

from django.conf import settings
from django.test import TestCase

from calls.tests.factories import CallRecordFactory
from calls.models import CallRecord


class CallRecordSignalsTest(TestCase):
    billings_url = settings.BILLINGS_URL

    @responses.activate
    @mock.patch('calls.signals.dispatch_new_completed_call_to_billing_service')
    def test_signal_was_called(self, mocked_call_record_handler):
        responses.add(
            method=responses.POST,
            url=self.billings_url,
            json={},
            status=201,
        )
        CallRecordFactory.create(record_type=CallRecord.START, call_id=2)
        CallRecordFactory.create(record_type=CallRecord.END, call_id=2)
        self.assertTrue(mocked_call_record_handler.called)
