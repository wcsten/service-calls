from datetime import datetime

from django.test import TestCase
from django.utils import timezone

from calls.tests.factories import CallRecordFactory
from calls.models import CallRecord


class CallRecordModelTest(TestCase):

    def test_create(self):
        timestamp = datetime(2019, 5, 12, 12, 0, 0, tzinfo=timezone.utc)
        call = CallRecordFactory.create(
            call_id=1,
            timestamp=timestamp
        )
        self.assertIsInstance(call, CallRecord)
