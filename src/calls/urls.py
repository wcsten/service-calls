from django.urls import path

from calls.views import CreateCallRecordAPIView

urlpatterns = [
    path('', CreateCallRecordAPIView.as_view(), name='calls'),
]
