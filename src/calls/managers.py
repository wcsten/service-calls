from django.db import models


class StartCallRecordManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().filter(record_type='start')


class EndCallRecordManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().filter(record_type='end')
