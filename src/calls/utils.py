import re


def is_phone_number_valid(number):
    return bool(re.match('^[0-9]{10,11}$', number))
