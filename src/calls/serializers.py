from rest_framework import serializers

from calls.utils import is_phone_number_valid
from calls.models import CallRecord


class CallRecordSerializer(serializers.ModelSerializer):
    source = serializers.CharField(required=False)
    destination = serializers.CharField(required=False)

    class Meta:
        model = CallRecord
        fields = [
            'record_type', 'timestamp', 'source', 'destination', 'call_id'
        ]

    def validate_source(self, value):
        if not is_phone_number_valid(value):
            raise serializers.ValidationError('Invalid phone number.')
        return value

    def validate_destination(self, value):
        if not is_phone_number_valid(value):
            raise serializers.ValidationError('Invalid phone number.')
        return value

    def validate(self, data):
        record_type = data.get('record_type')
        source = data.get('source')
        destination = data.get('destination')
        call_id = data.get('call_id')

        if record_type == CallRecord.START:
            if CallRecord.start_calls.filter(call_id=call_id).exists():
                raise serializers.ValidationError(
                    f'Call {call_id} has already been started.'
                )
            if not source or not destination:
                raise serializers.ValidationError(
                    f'Call {call_id} source and destination fields are required.'
                )
            if source == destination:
                raise serializers.ValidationError(
                    f'Call {call_id} source and destination can not be equals.'
                )
        else:
            if CallRecord.end_calls.filter(call_id=call_id).exists():
                raise serializers.ValidationError(
                    f'Call {call_id} has already been ended.'
                )
            if source or destination:
                raise serializers.ValidationError(
                    f'Call {call_id} end calls can not have destination or source.'
                )
        return data
