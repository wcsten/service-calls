install:
	@cd src/ && python manage.py migrate --settings=settings.settings
	@cd src/ && python manage.py runserver --settings=settings.settings

loaddata:
	@cd src/ && python manage.py loaddata call_records.json --settings=settings.settings

shell:
	@cd src/ && python manage.py shell --settings=settings.settings

test:
	@cd src/ && python manage.py test calls --settings=settings.settings

start:
	@cd src/ && python manage.py runserver --settings=settings.settings
